// ignore_for_file: library_private_types_in_public_api

import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:assessment/helpers/k_log.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:get/get.dart';

import '../base/base.dart';
import 'download_list_item.dart';

class DownloadFilePage extends StatefulWidget with WidgetsBindingObserver {
  final TargetPlatform? platform;
  final String title;

  const DownloadFilePage({
    super.key,
    required this.title,
    required this.platform,
  });

  @override
  _DownloadFilePageState createState() => _DownloadFilePageState();
}

class _DownloadFilePageState extends State<DownloadFilePage> {
  final ReceivePort _port = ReceivePort();

  @override
  void initState() {
    super.initState();
    Base.networkService;
    Base.permissionController.checkNotificationPermission();
    _bindBackgroundIsolate();

    FlutterDownloader.registerCallback(downloadCallback, step: 1);

    // _showContent = false;
    // _permissionReady = false;
    // _saveInPublicStorage = false;

    Base.downloadFileController.prepare();
  }

  @override
  void dispose() {
    _unbindBackgroundIsolate();
    super.dispose();
  }

  void _bindBackgroundIsolate() {
    final isSuccess = IsolateNameServer.registerPortWithName(
      _port.sendPort,
      'downloader_send_port',
    );
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      final taskId = (data as List<dynamic>)[0] as String;
      final status = DownloadTaskStatus.fromInt(data[1] as int);
      final progress = data[2] as int;

      kLog(
        'Callback on UI isolate: '
        'task ($taskId) is in status ($status) and process ($progress)',
      );

      if (Base.downloadFileController.tasksList.isNotEmpty) {
        final task = Base.downloadFileController.tasksList.firstWhere((task) => task.taskId == taskId);
        setState(() {
          task
            ..status = status
            ..progress = progress;
        });
      }
    });
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }

  @pragma('vm:entry-point')
  static void downloadCallback(
    String id,
    int status,
    int progress,
  ) {
    kLog(
      'Callback on background isolate: '
      'task ($id) is in status ($status) and process ($progress)',
    );

    IsolateNameServer.lookupPortByName('downloader_send_port')?.send([id, status, progress]);
  }

  Widget _buildDownloadList() {
    return Obx(() => ListView(
          padding: const EdgeInsets.symmetric(vertical: 16),
          children: [
            ...Base.downloadFileController.itemsList.map(
              (item) {
                final task = item.task;
                if (task == null) {
                  return Container(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    child: Text(
                      item.name!,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.blue,
                        fontSize: 18,
                      ),
                    ),
                  );
                }

                return DownloadListItem(
                  data: item,
                  onTap: (task) async {
                    final scaffoldMessenger = ScaffoldMessenger.of(context);

                    final success = await Base.downloadFileController.openDownloadedFile(task);
                    if (!success) {
                      scaffoldMessenger.showSnackBar(
                        const SnackBar(
                          content: Text('Cannot open this file'),
                        ),
                      );
                    }
                  },
                  onActionTap: (task) {
                    if (task.status! == DownloadTaskStatus.undefined) {
                      Base.downloadFileController.requestDownload(task);
                    } else if (task.status == DownloadTaskStatus.running) {
                      Base.downloadFileController.pauseDownload(task);
                    } else if (task.status == DownloadTaskStatus.paused) {
                      Base.downloadFileController.resumeDownload(task);
                    } else if (task.status == DownloadTaskStatus.complete || task.status == DownloadTaskStatus.canceled) {
                      Base.downloadFileController.delete(task);
                    } else if (task.status == DownloadTaskStatus.failed) {
                      Base.downloadFileController.retryDownload(task);
                    }
                  },
                  onCancel: Base.downloadFileController.delete,
                );
              },
            ),
          ],
        ));
  }

  Widget _buildNoPermissionWarning() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Text(
              'Grant storage permission to continue',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.blueGrey, fontSize: 18),
            ),
          ),
          const SizedBox(height: 32),
          TextButton(
            onPressed: Base.downloadFileController.retryRequestPermission,
            child: const Text(
              'Retry',
              style: TextStyle(
                color: Colors.blue,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          if (Platform.isIOS)
            PopupMenuButton<Function>(
              icon: const Icon(Icons.more_vert, color: Colors.white),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              itemBuilder: (context) => [
                PopupMenuItem(
                  onTap: () => exit(0),
                  child: const ListTile(
                    title: Text(
                      'Simulate App Backgrounded',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ],
            ),
        ],
      ),
      body: Builder(
        builder: (context) {
          if (!Base.downloadFileController.showContent.value) {
            return const Center(child: CircularProgressIndicator());
          }

          return Base.downloadFileController.permissionReady.value ? _buildDownloadList() : _buildNoPermissionWarning();
        },
      ),
    );
  }
}
