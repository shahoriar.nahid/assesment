import 'package:flutter/material.dart';

import '../models/character_model.dart';

class CharecterDetailsPage extends StatelessWidget {
  final CharacterModel item;
  const CharecterDetailsPage({required this.item, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Charecter Details Page'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: ClipRRect(
                clipBehavior: Clip.antiAlias,
                child: item.image != null && item.image != ''
                    ? Image.network(
                        item.image.toString(), // URL of the image
                        // Height of the image
                        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          } else {
                            return CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes!
                                  : null,
                            );
                          }
                        },
                      )
                    : const Icon(
                        Icons.person,
                        size: 34,
                      ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            RichText(
              text: TextSpan(
                children: [
                  const TextSpan(
                      text: 'Name: ',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      )),
                  TextSpan(
                      text: item.name ?? ' ',
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            RichText(
              text: TextSpan(
                children: [
                  const TextSpan(
                      text: 'Actor Name: ',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      )),
                  TextSpan(
                      text: item.actor ?? ' ',
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            RichText(
              text: TextSpan(
                children: [
                  const TextSpan(
                      text: 'House Name: ',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      )),
                  TextSpan(
                      text: item.house ?? ' ',
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
