import 'package:assessment/pages/charecter_details_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../base/base.dart';

class CharecterPage extends StatefulWidget {
  const CharecterPage({super.key});

  @override
  State<CharecterPage> createState() => _CharecterPageState();
}

class _CharecterPageState extends State<CharecterPage> {
  @override
  void initState() {
    super.initState();
    Base.taskTwoController.getCharacterData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
        title: const Text('Charecter Page'),
      ),
      body: Obx(
        () => SingleChildScrollView(
          child: Column(
            children: [
              ListView.builder(
                  padding: const EdgeInsets.all(0),
                  physics: const BouncingScrollPhysics(),
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  // primary: false,
                  itemCount: Base.taskTwoController.characterModel.length,
                  itemBuilder: (BuildContext context, int index) {
                    final item = Base.taskTwoController.characterModel[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: GestureDetector(
                        onTap: () {
                          Get.to(CharecterDetailsPage(item: item));
                        },
                        child: Container(
                          height: 30,
                          width: Get.width,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.shade500,
                                spreadRadius: 1,
                                blurRadius: 5,
                              )
                            ],
                          ),
                          child: Center(child: Text(item.name ?? '')),
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}



// Center(
//                       child: Image.network(
//                         'https://ik.imagekit.io/hpapi/harry.jpg', // URL of the image
//                         width: 200, // Width of the image
//                         height: 200, // Height of the image
//                         loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
//                           if (loadingProgress == null) {
//                             return child;
//                           } else {
//                             return CircularProgressIndicator(
//                               value: loadingProgress.expectedTotalBytes != null
//                                   ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes!
//                                   : null,
//                             );
//                           }
//                         },
//                       ),
//                     )