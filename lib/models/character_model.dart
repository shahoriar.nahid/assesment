// ignore: depend_on_referenced_packages
import 'package:json_annotation/json_annotation.dart';
part 'character_model.g.dart';

@JsonSerializable()
class CharacterModel {
  String? id;
  String? name;
  String? actor;
  String? house;
  String? image;

  CharacterModel({
    this.id,
    this.name,
    this.actor,
    this.house,
    this.image,
  });
  factory CharacterModel.fromJson(Map<String, dynamic> json) => _$CharacterModelFromJson(json);
  Map<String, dynamic> toJson() => _$CharacterModelToJson(this);
}
