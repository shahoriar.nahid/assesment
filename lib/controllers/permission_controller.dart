import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionController extends GetxController {
  void requestNotificationPermission() async {
    // Request permission
    PermissionStatus status = await Permission.notification.request();

    // Handle the permission status
    if (status.isGranted) {
      // Permission is granted
      // You can proceed with your notification-related tasks
    } else if (status.isDenied) {
      // Permission is denied
      // Handle the denied state, maybe show a dialog explaining why
    } else if (status.isPermanentlyDenied) {
      // Permission is permanently denied
      // You can open the app settings so the user can manually enable the permission
      openAppSettings();
    }
  }

  void checkNotificationPermission() async {
    // Check permission status
    PermissionStatus status = await Permission.notification.status;

    // Handle the permission status
    if (status.isGranted) {
      // Permission is already granted
      // You can proceed with your notification-related tasks
    } else {
      // Permission is not granted
      // Request permission
      requestNotificationPermission();
    }
  }
}
