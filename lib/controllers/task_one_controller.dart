import 'dart:io';
import 'package:assessment/helpers/k_log.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../base/base.dart';
import '../download_data.dart';
import '../models/download_data_model.dart';

class DownloadFileController extends GetxController {
  final saveInPublicStorage = RxBool(true);
  final showContent = RxBool(true);
  final permissionReady = RxBool(true);
  final localPath = RxString('');
  final tasksList = RxList<TaskInfo>();
  final itemsList = RxList<ItemHolder>();
  // List<TaskInfo>? _tasks;
  // late List<ItemHolder> _items;

  Future<void> prepare() async {
    final tasks = await FlutterDownloader.loadTasks();

    if (tasks == null) {
      kLog('No tasks were retrieved from the database.');
      return;
    }

    var count = 0;
    tasksList.clear();
    itemsList.clear();
    // tasksList = [];
    // _items = [];

    // tasksList.addAll(
    //   DownloadItems.documents.map(
    //     (document) => TaskInfo(name: document.name, link: document.url),
    //   ),
    // );

    // itemsList.add(ItemHolder(name: 'Documents'));
    // for (var i = count; i < tasksList.length; i++) {
    //   itemsList.add(ItemHolder(name: tasksList[i].name, task: tasksList[i]));
    //   count++;
    // }

    tasksList.addAll(
      DownloadItems.images.map((image) => TaskInfo(name: image.name, link: image.url)),
    );

    itemsList.add(ItemHolder(name: 'Images'));
    for (var i = count; i < tasksList.length; i++) {
      itemsList.add(ItemHolder(name: tasksList[i].name, task: tasksList[i]));
      count++;
    }

    tasksList.addAll(
      DownloadItems.videos.map((video) => TaskInfo(name: video.name, link: video.url)),
    );

    itemsList.add(ItemHolder(name: 'Videos'));
    for (var i = count; i < tasksList.length; i++) {
      itemsList.add(ItemHolder(name: tasksList[i].name, task: tasksList[i]));
      count++;
    }

    tasksList.addAll(
      DownloadItems.apks.map((video) => TaskInfo(name: video.name, link: video.url)),
    );

    itemsList.add(ItemHolder(name: 'APKs'));
    for (var i = count; i < tasksList.length; i++) {
      itemsList.add(ItemHolder(name: tasksList[i].name, task: tasksList[i]));
      count++;
    }

    for (final task in tasks) {
      for (final info in tasksList) {
        if (info.link == task.url) {
          info
            ..taskId = task.taskId
            ..status = task.status
            ..progress = task.progress;
        }
      }
    }

    permissionReady.value = await Base.downloadFileController.checkPermission();
    if (permissionReady.value) {
      await _prepareSaveDir();
    }

    showContent.value = true;
  }

  Future<void> _prepareSaveDir() async {
    localPath.value = (await _getSavedDir())!;
    final savedDir = Directory(localPath.value);
    if (!savedDir.existsSync()) {
      await savedDir.create();
    }
  }

  Future<String?> _getSavedDir() async {
    String? externalStorageDirPath;
    externalStorageDirPath = (await getApplicationDocumentsDirectory()).absolute.path;

    return externalStorageDirPath;
  }

  Future<void> delete(TaskInfo task) async {
    await FlutterDownloader.remove(
      taskId: task.taskId!,
      shouldDeleteContent: true,
    );
    await prepare();
  }

  Future<void> retryRequestPermission() async {
    final hasGranted = await Base.downloadFileController.checkPermission();

    if (hasGranted) {
      await _prepareSaveDir();
    }

    permissionReady.value = hasGranted;
  }

  Future<void> requestDownload(TaskInfo task) async {
    task.taskId = await FlutterDownloader.enqueue(
      url: task.link!,
      headers: {'auth': 'test_for_sql_encoding'},
      savedDir: localPath.value,
      saveInPublicStorage: true,
    );
  }

  Future<void> pauseDownload(TaskInfo task) async {
    await FlutterDownloader.pause(taskId: task.taskId!);
  }

  Future<void> resumeDownload(TaskInfo task) async {
    final newTaskId = await FlutterDownloader.resume(taskId: task.taskId!);
    task.taskId = newTaskId;
  }

  Future<void> retryDownload(TaskInfo task) async {
    final newTaskId = await FlutterDownloader.retry(taskId: task.taskId!);
    task.taskId = newTaskId;
  }

  Future<bool> openDownloadedFile(TaskInfo? task) async {
    final taskId = task?.taskId;
    if (taskId == null) {
      return false;
    }

    return FlutterDownloader.open(taskId: taskId);
  }

  Future<bool> checkPermission() async {
    if (Platform.isIOS) {
      return true;
    }

    if (Platform.isAndroid) {
      final info = await DeviceInfoPlugin().androidInfo;
      if (info.version.sdkInt > 28) {
        return true;
      }

      final status = await Permission.storage.status;
      if (status == PermissionStatus.granted) {
        return true;
      }

      final result = await Permission.storage.request();
      return result == PermissionStatus.granted;
    }

    throw StateError('unknown platform');
  }

  // url: 'http://ipv4.download.thinkbroadband.com/200MB.zip',
}
