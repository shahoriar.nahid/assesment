import 'package:dio/dio.dart';
import 'package:get/get.dart';

import '../helpers/k_log.dart';
import '../models/character_model.dart';

class TaskTwoController extends GetxController {
  final isLoading = RxBool(false);
  final characterModel = RxList<CharacterModel>();
  final dio = Dio();

  void getCharacterData() async {
    try {
      isLoading.value = true;
      final res = await dio.get('https://hp-api.onrender.com/api/characters');

      // final res = await get(path: 'https://hp-api.onrender.com/api/characters');
      kLog(res.data);

      // if (res.data['status'] == 200) {

      final data = res.data.map((json) => CharacterModel.fromJson(json as Map<String, dynamic>)).toList().cast<CharacterModel>()
          as List<CharacterModel>;

      if (data.isNotEmpty) {
        isLoading.value = false;
        characterModel.clear();
        characterModel.addAll(data);
        // }
      }
      isLoading.value = false;
    } catch (e, s) {
      kError(e);
      kError(s);
    } finally {
      isLoading.value = false;
    }
  }
}
