import 'package:get/get.dart';

import '../controllers/task_one_controller.dart';
import '../controllers/permission_controller.dart';
import '../controllers/task_two_controller.dart';
import '../services/network_service.dart';

class BaseBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NetworkService());
    Get.lazyPut(() => DownloadFileController());
    Get.lazyPut(() => TaskTwoController());
    Get.lazyPut(() => PermissionController());
  }
}
