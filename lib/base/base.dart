import 'package:get/get.dart';

import '../controllers/task_one_controller.dart';
import '../controllers/permission_controller.dart';
import '../controllers/task_two_controller.dart';
import '../services/network_service.dart';

class Base {
  Base._();
  static final networkService = Get.find<NetworkService>();
  static final downloadFileController = Get.find<DownloadFileController>();
  static final taskTwoController = Get.find<TaskTwoController>();
  static final permissionController = Get.find<PermissionController>();
}
